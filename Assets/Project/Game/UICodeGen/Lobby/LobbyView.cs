// ===================================
// PlayCore Code Gen
// OverWrite : False
// ===================================

using UnityEngine;
using System;
using System.Collections.Generic;

namespace PlayCore.UI
{
    public class LobbyView : UIBasePopupView<LobbyModel>
    {
        public override void OnEnter(LobbyModel model)
        {
            Debug.Log(typeof(LobbyState) + "View");
            base.OnEnter(model);
            OnRefresh();
        }

        public override void OnExit()
        {
            base.OnExit();
        }

        public override void OnRefresh()
        {
            base.OnRefresh();
        }
    }
}

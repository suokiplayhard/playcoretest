// ===================================
// PlayCore Code Gen
// OverWrite : False
// ===================================

using UnityEngine;
using System;
using System.Collections.Generic;

namespace PlayCore.UI
{
    public class LobbyParam : UIBasePopupParam<LobbyParam>
    {
        public override void Initialize()
        {
            base.Initialize();
            popupName = "Lobby";
            popupPath = "";
        }

        public override void OnExit()
        {
            base.OnExit();
            Dispose(this);
        }
    }

    public class LobbyModel : UIBasePopupModel
    {
        public LobbyParam Param { get { return BaseParam as LobbyParam; } }

        public void OnEnter(LobbyParam param)
        {
            base.OnEnter(param);
            Debug.Log(typeof(LobbyState) + "Model");
        }

        public override void OnExit()
        {
            base.OnExit();
        }
    }
}

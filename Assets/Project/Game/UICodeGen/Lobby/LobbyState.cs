// ===================================
// PlayCore Code Gen
// OverWrite : False
// ===================================

using UnityEngine;
using System;
using System.Collections.Generic;

namespace PlayCore.UI
{
    public class LobbyState : UIBasePopupState<LobbyModel, LobbyView>
    {
        public override void OnEnterModel()
        {
            base.OnEnterModel();
            if (StateParams.IsType<LobbyParam>())
                Model.OnEnter(StateParams.Cast<LobbyParam>().item1);
            else
                Model.OnEnter(LobbyParam.Create());
        }

        public override void OnEnter()
        {
            Debug.Log(typeof(LobbyState) + "State");
            base.OnEnter();
        }

        public override void OnExit()
        {
            base.OnExit();
        }

        public override void OnEvent(EventMessage msg)
        {
            base.OnEvent(msg);

            switch (msg.Msg)
            {
            }
        }
    }
}

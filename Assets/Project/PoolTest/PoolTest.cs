using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolTest : MonoBehaviour
{
    public Queue<Bullet> bullets = new Queue<Bullet>();

    private void Awake()
    {
        PoolMan.Instance.Init();
    }

    void Start()
    {
        InvokeRepeating( nameof(Spawn), 1f, 1f );
        InvokeRepeating( nameof(Despawn), 1f, 2f );
    }

    public void Spawn()
    {
        var bullet = PoolMan.Spawn<Bullet>( nameof(Bullet), PoolName.Actor, transform );
        bullets.Enqueue( bullet );
    }

    public void Despawn()
    {
        var bullet = bullets.Dequeue();
        PoolMan.Despawn( PoolName.Actor, bullet );
    }

    public void DespawnAll()
    {
        PoolMan.DespawnAll( PoolName.Actor );
    }

}
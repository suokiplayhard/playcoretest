using System;
using System.Collections;
using System.Collections.Generic;
using PlayCore;
using UnityEngine;

public class Bullet : MonoBehaviour, IPooler
{
    //씬을 정리해야하는데.
    //모든 풀객채를 반환은 안할거고
    //풀그룹 A, B,    a 반환도아니고 b반환도아니고
    // a에서 T,  B  C  에들을 풀을 반환해야한다.
    
    //반환되야할 특정그룹이 있다...동적 그룹.
    //List<Component> 
    
    //Bullet. 
    //  1,2,3,4,5 
    //  1,2,3  꺼내서 씬에서 총알이 날아감.
    //  4,5 만 가능.
    // 씬전환.
    //  1,2,3 을 수동으로 반환해야함.
    
    // pooled 로 관리되고있는애들도 반환.  
    // 모든객체가 삭제.
    
    //풀에 100개를 담아놧는데
    //사용자가 2시간동안 이 객채를 안씀.
    //100개를 계속 들고있는가?
    //..있으면 좋을듯.
    
    
    //  디스폰을 안하면 에러는 안나지만 null 로 pooled 에 쌓이는중.
    //  null 을 지우는 기능 추가....
    
    // Update is called once per frame
    void Update()
    {
        transform.Translate( Vector3.up * Time.deltaTime * 10f );
    }

    public void OnGenerate()
    {
        //Todo OnGenerate
    }
}
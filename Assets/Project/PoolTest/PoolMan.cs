using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayCore;

public class PoolName
{
    public const string UI = "Pool_UI";
    public const string Actor = "Pool_Actor";
}

public class PoolMan : MonoSingletonWithCreateDontDestory<PoolMan>
{
    public Dictionary<string, Pool> pools = new Dictionary<string, Pool>();
    
    public static Pool UIPool { get { return Instance.pools[PoolName.UI]; } }
    public static Pool ActorPool { get { return Instance.pools[PoolName.Actor]; } }
    
    //풀 그룹 이름
    //풀 그룹 스크립트 이름.
    //풀안에는 스포너.
    // 스포너가 생성할때는 컴퍼넌트이름으로 디셔너리 관리함.
    

    public void Init()
    {
        InitPool(PoolName.UI);
        InitPool(PoolName.Actor);
    }

    void InitPool(string poolName)
    {
        //여기서 poolname 은 프리팹이름인듯..
        Pool pool = Util.LoadInstantiate<Pool>( "Pool/" + poolName  );
        if( pool != null ) {
            pool.gameObject.SetActive( true );
            //풀 그룹을 생성하고 그 자식으로 붙여서 트랜스폼 초기화.
            Util.TransformIdentityLocal( this.transform, pool.transform );
            pool.Initialize();
            pools.Add( poolName, pool );
        }
    }

    public static T Spawn<T>( string prefabName, string poolName, Transform transform ) where T : Component
    {
        return PoolMan.Instance.pools[poolName].Spawn<T>( prefabName, transform );
    }
    
    public static void Despawn<T>( string poolName, T t  ) where T : Component
    {
        PoolMan.Instance.pools[poolName].Despawn( t );
    }
    
    public static void DespawnAll( string poolName )
    {
        PoolMan.Instance.pools[poolName].DespawnAll( poolName );
    }
}

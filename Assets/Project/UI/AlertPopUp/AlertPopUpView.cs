// ===================================
// PlayCore Code Gen
// ===================================

using UnityEngine;
using System;
using System.Collections.Generic;

namespace PlayCore.UI
{
    public class AlertPopUpView : UIBasePopupView<AlertPopUpModel>
    {
        public override void OnEnter(AlertPopUpModel model)
        {
            Debug.Log(typeof(AlertPopUpState) + "View");
            base.OnEnter(model);
            OnRefresh();
        }

        public override void OnExit()
        {
            base.OnExit();
        }

        public override void OnRefresh()
        {
            base.OnRefresh();
        }
    }
}

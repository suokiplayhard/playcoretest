// ===================================
// PlayCore Code Gen
// ===================================

using UnityEngine;
using System;
using System.Collections.Generic;

namespace PlayCore.UI
{
    public class AlertPopUpParam : UIBasePopupParam<AlertPopUpParam>
    {
        public override void Initialize()
        {
            base.Initialize();
            popupName = "AlertPopUp";
        }

        public override void OnExit()
        {
            base.OnExit();
            Dispose(this);
        }
    }

    public class AlertPopUpModel : UIBasePopupModel
    {
        public AlertPopUpParam Param { get { return BaseParam as AlertPopUpParam; } }

        public void OnEnter(AlertPopUpParam param)
        {
            base.OnEnter(param);
            Debug.Log(typeof(AlertPopUpState) + "Model");
        }

        public override void OnExit()
        {
            base.OnExit();
        }
    }
}

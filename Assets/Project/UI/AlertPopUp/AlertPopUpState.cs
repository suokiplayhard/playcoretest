// ===================================
// PlayCore Code Gen
// ===================================

using UnityEngine;
using System;
using System.Collections.Generic;

namespace PlayCore.UI
{
    public class AlertPopUpState : UIBasePopupState<AlertPopUpModel, AlertPopUpView>
    {
        public override void OnEnterModel()
        {
            base.OnEnterModel();
            if (StateParams.IsType<AlertPopUpParam>())
                Model.OnEnter(StateParams.Cast<AlertPopUpParam>().item1);
            else
                Model.OnEnter(AlertPopUpParam.Create());
        }

        public override void OnEnter()
        {
            Debug.Log(typeof(AlertPopUpState) + "State");
            base.OnEnter();
        }

        public override void OnExit()
        {
            base.OnExit();
        }

        public override void OnEvent(EventMessage msg)
        {
            base.OnEvent(msg);

            switch (msg.Msg)
            {
            }
        }
    }
}

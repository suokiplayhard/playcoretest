// ===================================
// PlayCore Code Gen
// OverWrite : True
// ===================================

using UnityEngine;
using System;
using System.Collections.Generic;

namespace PlayCore.Table
{
    public class TableItemKeys
    {
        public const string a_01 = "a_01";
        public const string a_02 = "a_02";
        public const string a_03 = "a_03";
        public const string a_04 = "a_04";
        public const string a_05 = "a_05";
        public const string a_06 = "a_06";
        public const string a_07 = "a_07";
        public const string a_08 = "a_08";
        public const string a_09 = "a_09";
        public const string a_10 = "a_10";
        public const string a_11 = "a_11";
        public const string a_12 = "a_12";
    }
}

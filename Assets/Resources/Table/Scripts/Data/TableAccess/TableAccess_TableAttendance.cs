// ===================================
// PlayCore Code Gen
// OverWrite : True
// ===================================

using UnityEngine;
using System;
using System.Collections.Generic;

namespace PlayCore.Table
{
    public partial class TableAccess
    {
        private static TableAttendanceSchema tableTableAttendanceSchema;
        private static Dictionary<string, TableAttendance> TableAttendanceDictionary
        {
            get
            {
                var schema = GetTableAttendanceSchema();
                if(schema == null)
                    return null;

                return schema.tableDatas;
            }
        }

        public static TableAttendanceSchema GetTableAttendanceSchema()
        {
            if(tableTableAttendanceSchema == null)
            {
                tableTableAttendanceSchema = Resources.Load<TableAttendanceSchema>("TableAttendanceSchema");
                if (tableTableAttendanceSchema != null)
                {
                    foreach (var pair in tableTableAttendanceSchema.tableDatas)
                        pair.Value.Initialize();
                }
            }

            return tableTableAttendanceSchema;
        }

        public static Dictionary<string, TableAttendance> GetTableAttendanceDictionary()
        {
            return TableAttendanceDictionary;
        }

        public static TableAttendance GetTableAttendance(string key)
        {
            if (TableAttendanceDictionary == null)
                return null;

            TableAttendance value;
            if (TableAttendanceDictionary.TryGetValue(key, out value) == false)
            {
                Debug.LogWarning("Not Find TableAttendanceData // KeyName : " + key);
                return null;
            }

            return value;
        }
    }
}

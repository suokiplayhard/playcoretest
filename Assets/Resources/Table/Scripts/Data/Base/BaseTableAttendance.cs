// ===================================
// PlayCore Code Gen
// OverWrite : True
// ===================================

using UnityEngine;
using System;
using System.Collections.Generic;

namespace PlayCore.Table
{
    [Serializable]
    public abstract class BaseTableAttendance : ITableData
    {
        public BaseTableAttendance() : base()
        {
        }

        public BaseTableAttendance(string key) : base(key)
        {
        }

        public virtual void Initialize()
        {
        }

        [SerializeField]
        private int _day;
        public int day
        {
            get { return _day; }
        }

        [SerializeField]
        private string _reward;
        public string reward
        {
            get { return _reward; }
        }

        [SerializeField]
        private float _rewardAmount;
        public float rewardAmount
        {
            get { return _rewardAmount; }
        }

        [SerializeField]
        private Animal _Animal;
        public Animal Animal
        {
            get { return _Animal; }
        }

        [SerializeField]
        private List<string> _members;
        public List<string> members
        {
            get { return _members; }
        }


        public override void SetData(IList<object> list)
		{
            _key = DBHelper.GetString(list[0], _key);
            _day = DBHelper.GetInt(list[1], _day);
            _reward = DBHelper.GetString(list[2], _reward);
            _rewardAmount = DBHelper.GetFloat(list[3], _rewardAmount);
            _Animal = DBHelper.GetEnum(list[4], _Animal);
            _members = DBHelper.GetStringList(list[5], _members);
        }
    }
}

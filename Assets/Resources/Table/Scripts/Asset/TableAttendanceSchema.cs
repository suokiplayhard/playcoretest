// ===================================
// PlayCore Code Gen
// OverWrite : True
// ===================================

using UnityEngine;
using System;

namespace PlayCore.Table
{
    public class TableAttendanceSchema : ITableSchema<TableAttendance>
    {
        [SerializeField]
        private SerializeDictionaryString<TableAttendance> _tableDatas;
        public override SerializableDictionary<string, TableAttendance> tableDatas => _tableDatas;
    }
}
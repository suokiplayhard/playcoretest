using System.Collections;
using System.Collections.Generic;
using PlayCore;
using UnityEngine;

public class SeqObjectSpawner : MonoBehaviour
{
    public Transform target;
    // Update is called once per frame
    void Update()
    {
        if( Input.GetKeyDown( KeyCode.A ) )
        {
            GenSeqObject();
        }
    }

    void GenSeqObject()
    {
        var seq = PoolMan.Spawn<SequenceObject>( nameof(SequenceObject), PoolName.Actor, this.transform );
        seq.Play( transform.position, target.position,
            (obj) =>
            {
                PoolMan.Despawn( PoolName.Actor, seq );
            } );
    }
}